//
//  EmojiArtDocumentView.swift
//  EmojiArt
//
//  Created by Роман Хоменко on 06.03.2022.
//

import SwiftUI

struct EmojiArtDocumentView: View {
    @ObservedObject var document: EmojiArtDocument
    
    var defaultEmojiFontSize: CGFloat = 40
    @State var emojiIndexesWithSelection: Set<EmojiArtModel.Emoji> = []
    
    var body: some View {
        VStack(spacing: 0) {
            ZStack(alignment: .topLeading) {
                documentBody
                deletSelectedButton
                    .padding(.vertical)
                    .transition(.opacity)
            }
            PaletteChoser(emojiFontSize: defaultEmojiFontSize)
        }
    }
    
    var documentBody: some View {
        GeometryReader { geometry in
            ZStack {
                Color.white.overlay {
                    OptionalImage(uiImage: document.backgroundImage)
                        .scaleEffect(zoomScale)
                        .position(convertFromEmojiCoordinates((0, 0), in: geometry))
                }
                .gesture(doubleTapToZoom(in: geometry.size).exclusively(before: oneTapToDiselectAllEmoji()))
                if document.backgroundImageFetchStatus == .fetching {
                    ProgressView().scaleEffect(2)
                } else {
                    ForEach(document.emojis) { emoji in
                        Text(emoji.text)
                            .border(Color.black, width: emojiIndexesWithSelection.containsMatching(emoji) ? 1 : 0)
                            .font(.system(size: fontSize(for: emoji)))
                            .scaleEffect(scale(for: emoji))
                            .scaleEffect(zoomScale)
                            .position(position(for: emoji, in: geometry))
                            .offset(emojiIndexesWithSelection.containsMatching(emoji) ? gestureEmojiPanOffset : .zero)
                            .onTapGesture {
                                emojiIndexesWithSelection.toggleMatching(emoji)
                            }
                            .gesture(panGesture(for: emoji))
                    }
                }
            }
            .clipped()
            .onDrop(of: [.plainText, .url, .image], isTargeted: nil) { providers, location in
                drop(providers: providers, at: location, in: geometry)
            }
            .gesture(panGesture().simultaneously(with: zoomGesture())) // sequence of two gestures
            .alert(item: $alertToShow) { alertToShow in
                // return Alert
                alertToShow.alert()
            }
            .onChange(of: document.backgroundImageFetchStatus) { status in
                switch status {
                case .failed(let url):
                    showBackgroundImageFetchedFailedAlert(url)
                default:
                    break
                }
            }
        }
    }
    
    @State private var alertToShow: IdentifiableAlert?
    
    private func showBackgroundImageFetchedFailedAlert(_ url: URL) {
        alertToShow = IdentifiableAlert(id: "fetched failed" + url.absoluteString, alert: {
            Alert(
                title: Text("Background Image Fetched"),
                message: Text("Couldn't load image from \(url)."),
                dismissButton: .default(Text("OK"))
            )
        })
    }
    
    private func drop(providers: [NSItemProvider], at location: CGPoint, in geometry: GeometryProxy) -> Bool {
        let newEmojiCoordinate = convertToEmojiCoordinates(location, in: geometry)
        
        // for URL drop
        var found = providers.loadObjects(ofType: URL.self) { url in
            document.setBackground(.url(url.imageURL)) // from URL extention: url.imageURL doesn't allow Xcode to expand url
        }
        if !found {
            // for Image drop
            found = providers.loadObjects(ofType: UIImage.self) { image in
                if let data = image.jpegData(compressionQuality: 1.0) {
                    document.setBackground(.imageData(data))
                }
            }
            
        }
        if !found {
            // for add emoji on the document and move it without copy
            found = providers.loadObjects(ofType: String.self) { string in
                if let emoji = string.first, emoji.isEmoji {
                    for index in document.emojis {
                        if Character(document.emojis[index].text) == emoji {
                            document.moveEmoji(document.emojis[index], by: CGSize(width: newEmojiCoordinate.x, height: newEmojiCoordinate.y))
                            return
                        }
                    }
                    document.addEmoji(
                        String(emoji),
                        at: newEmojiCoordinate,
                        size: defaultEmojiFontSize / zoomScale
                    )
                }
            }
        }
        return found
    }
    
    private func position(for emoji: EmojiArtModel.Emoji, in geometry: GeometryProxy) -> CGPoint {
        convertFromEmojiCoordinates((emoji.x, emoji.y), in: geometry)
    }
    
    private func convertToEmojiCoordinates(_ location: CGPoint, in geometry: GeometryProxy) -> (x: Int, y: Int) {
        let center = geometry.frame(in: .local).center
        let location = CGPoint(
            x: (location.x - panOffset.width - center.x) / zoomScale,
            y: (location.y - panOffset.height - center.y) / zoomScale
        )
        return (Int(location.x), Int(location.y))
    }
    
    private func convertFromEmojiCoordinates(_ location: (x: Int, y: Int), in geometry: GeometryProxy) -> CGPoint {
        let center = geometry.frame(in: .local).center
        return CGPoint(
            x: center.x + CGFloat(location.x) * zoomScale + panOffset.width,
            y: center.y + CGFloat(location.y) * zoomScale + panOffset.height
        )
    }
    
    private func fontSize(for emoji: EmojiArtModel.Emoji) -> CGFloat {
        CGFloat(emoji.size)
    }
    
    private func oneTapToDiselectAllEmoji() -> some Gesture {
        return TapGesture(count: 1)
            .onEnded {
                withAnimation {
                    emojiIndexesWithSelection.removeAll()
                }
            }
    }
    
    private func doubleTapToZoom(in size: CGSize) -> some Gesture {
        return TapGesture(count: 2)
            .onEnded {
                withAnimation {
                    zoomToFit(document.backgroundImage, in: size)
                }
            }
    }
    
    @State private var steadyStatePanOffset: CGSize = CGSize.zero
    @GestureState private var gesturePanOffset: CGSize = CGSize.zero
    @GestureState private var gestureEmojiPanOffset: CGSize = CGSize.zero
    
    private var panOffset: CGSize {
        (steadyStatePanOffset + gesturePanOffset) * zoomScale
    }
    
    private func panGesture(for emoji: EmojiArtModel.Emoji? = nil) -> some Gesture {
        if let emoji = emoji, emojiIndexesWithSelection.containsMatching(emoji) {
            // if drag gesture starts on selected emoji
            return DragGesture()
                .updating($gestureEmojiPanOffset) { latestDragGestureValue, gestureEmojiPanOffset, _ in
                    gestureEmojiPanOffset = latestDragGestureValue.translation
                }
                .onEnded { gestureEmojiPannOffsetAtTheEnd in
                    for emoji in emojiIndexesWithSelection {
                        document.moveEmoji(emoji, by: gestureEmojiPannOffsetAtTheEnd.translation / zoomScale)
                    }
                }
        } else {
            // if drad gesture starts on unselected emoji or on the background
            return DragGesture()
                .updating($gesturePanOffset) { latestDragGestureValue, gesturePanOffset, _ in
                    gesturePanOffset = latestDragGestureValue.translation / zoomScale
                }
                .onEnded { finalDragGestureValue in
                    steadyStatePanOffset = steadyStatePanOffset + (finalDragGestureValue.translation / zoomScale)
                }
        }
    }
    
    @State private var steadyStateZoomScale: CGFloat = 1
    @GestureState private var gestureZoomScale: CGFloat = 1
    @GestureState private var gestureEmojiZoomScale: CGFloat = 1
    
    private var zoomScale: CGFloat {
        steadyStateZoomScale * gestureZoomScale
    }
    
    private func scale(for emoji: EmojiArtModel.Emoji) -> CGFloat {
        emojiIndexesWithSelection.containsMatching(emoji) ? (zoomScale * gestureEmojiZoomScale) : zoomScale
    }
    
    private func zoomGesture() -> some Gesture {
        return MagnificationGesture()
            .updating($gestureZoomScale) { latestGestureScale, gestureZoomScale, _ in
                if emojiIndexesWithSelection.isEmpty {
                    gestureZoomScale = latestGestureScale
                }
            }
            .updating($gestureEmojiZoomScale) { latestEmojyGestureScale, gestureEmojiZoomScale, _ in
                if !emojiIndexesWithSelection.isEmpty {
                    gestureEmojiZoomScale = latestEmojyGestureScale
                }
            }
            .onEnded { gestureScaleAtTheEnd in
                if emojiIndexesWithSelection.isEmpty {
                    steadyStateZoomScale *= gestureScaleAtTheEnd
                } else {
                    for emoji in emojiIndexesWithSelection {
                        document.scaleEmoji(emoji, by: gestureScaleAtTheEnd)
                    }
                }
            }
    }
    
    private func zoomToFit(_ image: UIImage?, in size: CGSize) {
        if let image = image, image.size.width > 0, image.size.height > 0, size.width > 0, size.height > 0 { // chek image for nil
            let hZoom = size.width / image.size.width
            let vZoom = size.height / image.size.height
            steadyStatePanOffset = .zero
            steadyStateZoomScale = min(hZoom, vZoom)
        }
    }
    
    @ViewBuilder
    var deletSelectedButton: some View {
            if !emojiIndexesWithSelection.isEmpty {
            Button {
                withAnimation {
                    for emoji in emojiIndexesWithSelection {
                        document.deleteEmoji(emoji)
                    }
                    emojiIndexesWithSelection.removeAll()
                }
            } label: {
                Label("Delete selected Emoji", systemImage: "trash")
                    .font(.title2.bold())
                    .foregroundColor(.white)
                    .padding(8)
                    .background(RoundedRectangle(cornerRadius: 10))
                    .foregroundColor(.red)
            }
            .padding(.horizontal)
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        EmojiArtDocumentView(document: EmojiArtDocument())
    }
}
