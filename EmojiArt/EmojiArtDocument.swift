//
//  EmojiArtDocument.swift
//  EmojiArt
//
//  Created by Роман Хоменко on 06.03.2022.
//

// MARK: - Required Tasks:
// [x] - support selection one or more emoji on the document
// [x] - support selection and diselection by one tap on emoji
// [x] - support diselection of all emoji by tapping on the background
// [x] - support scaling for all chosen emoji at a time
// [x] - if there is no selection, then entire doceument should be scaled
// [x] - support deleting emoji from document

// MARK: - Extra Tasks:
// [] - if there are some selected emoji and you drag unselected emoji, you should
// move only that emoji
// [] - Figure out how to make AnimatableSystemFontModifier that will animate the size of a system font and use that instead of the combination of .font and .scaleEffect
// In other words, make your own ViewModifier

import SwiftUI

class EmojiArtDocument: ObservableObject {

    @Published private(set) var emojiArt: EmojiArtModel {
        didSet {
            scheduleAutoSave()
            if emojiArt.background != oldValue.background {
                fetchBackgroundImageData()
            }
        }
    }
    
    private var autosaveTimer: Timer?
    
    private func scheduleAutoSave() {
        autosaveTimer?.invalidate()
        autosaveTimer = Timer.scheduledTimer(withTimeInterval: Autosave.coalescingInterval, repeats: false) { _ in
            self.autoSave()
        }
    }
    
    private struct Autosave {
        static let filename = "Autosaved.emojiart"
        static var url: URL? {
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            return documentDirectory?.appendingPathComponent(filename)
        }
        static let coalescingInterval = 5.0
    }
    
    private func autoSave() {
        if let url = Autosave.url {
            save(to: url)
        }
    }
    
    private func save(to url: URL) {
        let thisFunction = "\(String(describing: self)).\(#function)"
        do {
            let data: Data = try emojiArt.json()
            print("\(thisFunction) json = \(String(data: data, encoding: .utf8) ?? "nil")")
            try data.write(to: url)
            print("\(thisFunction) success!")
        } catch let encodingError where encodingError is EncodingError {
            print("\(thisFunction) couldn't rncode EmojiArt as JSON because \(encodingError.localizedDescription)")
        } catch let error {
            print("\(thisFunction) error = \(error)")
        }
    }
    
    init() {
        if let url = Autosave.url, let autosaveEmojiArt = try? EmojiArtModel(url: url) {
            emojiArt = autosaveEmojiArt
            fetchBackgroundImageData()
        } else {
            emojiArt = EmojiArtModel()
        }
    }
    
    var emojis: [EmojiArtModel.Emoji] { emojiArt.emojis }
    var background: EmojiArtModel.Background { emojiArt.background }
    
    @Published var backgroundImage: UIImage?
    @Published var backgroundImageFetchStatus = BackgroundImageFechStatus.idle
    
    enum BackgroundImageFechStatus: Equatable {
        case idle
        case fetching
        case failed(URL)
    }
    
    private func fetchBackgroundImageData() {
        backgroundImage = nil
        switch emojiArt.background {
        case .url(let url):
            // fetch the url
            backgroundImageFetchStatus = .fetching
            DispatchQueue.global(qos: .userInitiated).async {
                let imageData = try? Data(contentsOf: url)
                DispatchQueue.main.async { [weak self] in
                    if self?.emojiArt.background == EmojiArtModel.Background.url(url) { // chek on a new file loads or old
                        self?.backgroundImageFetchStatus = .idle
                        if imageData != nil {
                            self?.backgroundImage = UIImage(data: imageData!)
                        }
                        if self?.backgroundImage == nil {
                            self?.backgroundImageFetchStatus = .failed(url)
                        }
                    }
                }
            }
        case .imageData(let data):
            backgroundImage = UIImage(data: data)
        case .blank:
            break
        }
    }
    
    // MARK: - Intent(s)
    
    func setBackground(_ background: EmojiArtModel.Background) {
        emojiArt.background = background
    }
    
    func addEmoji(_ emoji: String, at location: (x: Int, y: Int), size: CGFloat) {
        emojiArt.addEmoji(emoji, at: location, size: Int(size))
    }
    
    func moveEmoji(_ emoji: EmojiArtModel.Emoji, by offset: CGSize) {
        if let index = emojiArt.emojis.index(matching: emoji) {
            emojiArt.emojis[index].x += Int(offset.width)
            emojiArt.emojis[index].y += Int(offset.height)
        }
    }
    
    func scaleEmoji(_ emoji: EmojiArtModel.Emoji, by scale: CGFloat) {
        if let index = emojiArt.emojis.index(matching: emoji) {
            emojiArt.emojis[index].size = Int((CGFloat(emojiArt.emojis[index].size) * scale).rounded(.toNearestOrAwayFromZero))
        }
    }
    
    func deleteEmoji(_ emoji: EmojiArtModel.Emoji) {
        emojiArt.deleteEmoji(emoji: emoji)
    }
}

extension Set where Element: Identifiable {
    mutating func toggleMatching(_ element: Element) {
        if let mathcingIndex = firstIndex(where: { $0.id == element.id }) {
            remove(at: mathcingIndex)
        } else {
            insert(element)
        }
    }
    
    func containsMatching(_ element: Element) -> Bool {
        contains(where: { $0.id == element.id })
    }
}
